# Variables
skuid=`getprop ro.cda.skuid.id_final`
sku=${skuid:3:2}
brand=`getprop ro.product.brand`
device=`getprop ro.product.device`
platform=`getprop ro.board.platform`
treble=`getprop ro.treble.enabled`

ui_print " "
ui_print "  Brand       : $brand"
ui_print "  Codename    : $device"
ui_print "  SoC         : $platform"
ui_print " "
# Checks


ui_print " ___________________________________________"
ui_print "|                                           |"          
ui_print "|          Universal Kernel Tweaks          |"
ui_print "|                   by                      |"
ui_print "|            enweazudaniel @XDA             |"
ui_print "|___________________________________________|"
ui_print ""

# Choices
ui_print "  From the modes listed below,"
ui_print "  use the volume keys to choose the tweaks mode"
ui_print " "
ui_print "  Profiles: "
ui_print " "
ui_print "   Power save"
ui_print "   Balanced(Default)"
ui_print "  "
sleep 1

ui_print "  Vol(+) = Power save "
ui_print "  Vol(-) = Balanced..."
ui_print " "
if $VKSEL; then
ui_print "  Power save profile selected."
ui_print " "
   MODE="BAT"
   else
MODE=""
fi

if [ -z $MODE ]; then
ui_print "  Vol(+) = Balanced"
ui_print ""
ui_print " "
if [ $VKSEL ]; then
ui_print "  Balanced profile selected."
ui_print " "
   MODE="BAL"
   else
MODE=""
fi
fi



if [ -z $MODE ]; then
ui_print "  Vol(+) = Performance"
ui_print "  Vol(-) = Show more options.."
ui_print " "
if [ $VKSEL ]; then
ui_print "  Performance profile selected."
ui_print " "
MODE="PER"
else
MODE=""
fi
fi

if [ -z $MODE ]; then
ui_print "  Vol(+) = Fast"
ui_print " "
if [ $VKSEL ]; then
ui_print "  Fast profile selected."
ui_print " "
MODE="TUR"
else
MODE="BAL"
ui_print "  Sorry Incorrect profile selected Default profile will be applied."
ui_print "  Balance profile selected by default."
ui_print " "
fi
fi

if [ $MODE = 'BAT' ]; then
PROFILEMODE=0
elif [ $MODE = 'BAL' ]; then
PROFILEMODE=1
elif [ $MODE = 'PER' ]; then
PROFILEMODE=2
elif [ $MODE = 'TUR' ]; then
PROFILEMODE=3
fi

case "$platform" in
  "msmnile")
    ui_print "- Extracting module files"
    cp_ch -r $TMPDIR/profiles/sdm855/system $MODPATH/system 0664 > /dev/null 2>&1
    cp_ch -r $MODPATH/profiles/sdm855/system $MODPATH/system 0664 > /dev/null 2>&1
	
  ;;
  "sdm845")
    ui_print "- Extracting module files"
    cp_ch -r $TMPDIR/profiles/sdm845/system $MODPATH/system 0664 > /dev/null 2>&1
    cp_ch -r $MODPATH/profiles/sdm845/system $MODPATH/system 0664 > /dev/null 2>&1

  ;;
  "sm6150")
    ui_print "- Extracting module files"
    cp_ch -r $TMPDIR/profiles/sdm675_730/system $MODPATH/system 0664 > /dev/null 2>&1
    cp_ch -r $MODPATH/profiles/sdm675_730/system $MODPATH/system 0664 > /dev/null 2>&1


  ;;
  "sdm710")
    ui_print "- Extracting module files"
    cp_ch -r $TMPDIR/profiles/sdm710/system $MODPATH/system 0664 > /dev/null 2>&1
    cp_ch -r $MODPATH/profiles/sdm710/system $MODPATH/system 0664 > /dev/null 2>&1


  ;;
  "msm8998")
    ui_print "- Extracting module files"
    cp_ch -r $TMPDIR/profiles/msm8998/system $MODPATH/system 0664 > /dev/null 2>&1
    cp_ch -r $MODPATH/profiles/msm8998/system $MODPATH/system 0664 > /dev/null 2>&1

  ;;
   "msm8996")
    ui_print "- Extracting module files"
    cp_ch -r $TMPDIR/profiles/msm8996/system $MODPATH/system 0664 > /dev/null 2>&1
    cp_ch -r $MODPATH/profiles/msm8996/system $MODPATH/system 0664 > /dev/null 2>&1
  ;;
esac

rm -rf $MODPATH/profiles/

  
  VER=$(cat ${TMPDIR}/module.prop | grep -oE 'version=v[0-9].[0-9]+' | awk -F= '{ print $2 }' )
  
  sed -i "s/<VER>/${VER}/g" $TMPDIR/service.sh > /dev/null 2>&1
  sed -i "s/<PROFILEMODE>/$PROFILEMODE/g" $TMPDIR/service.sh > /dev/null 2>&1
  sed -i "s/<VER>/${VER}/g" $MODPATH/service.sh > /dev/null 2>&1
  sed -i "s/<PROFILEMODE>/$PROFILEMODE/g" $MODPATH/service.sh > /dev/null 2>&1
  
ui_print " "
ui_print "  Join Official Telegram Group @tweakmypixel"
ui_print " "
